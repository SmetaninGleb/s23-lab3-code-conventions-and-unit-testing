package com.hw.db.controllers;

import java.sql.Timestamp;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.mockito.Mockito;
import org.mockito.MockedStatic;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.BeforeEach;
import com.hw.db.DAO.*;
import com.hw.db.models.*;
import com.hw.db.models.Thread;

class ThreadControllerTests {
    String tempNick = "Foo";
    String tempSlug = "lab3";
    String tempForum = "lab3-forum";
    int tempThreadId = 1;

    ArrayList<Thread> threadList;
    Thread tempThread;
    ArrayList<Post> postList;
    Post tempPost;
    User tempUser;
    ThreadController threadController;

    @BeforeEach
    void setUp() {
        threadController = new ThreadController();
        Timestamp timestamp = new Timestamp(100000);
        tempUser = new User(tempNick, "email@mail.example", "Foo Boo", "Foo Boo about");

        tempPost = new Post(tempNick, timestamp, tempForum, "I am Foo Boo", 0, 0, false);
        postList = new ArrayList<>();
        postList.add(tempPost);

        tempThread = new Thread(tempNick, timestamp, tempForum, "It is my thread", tempSlug, "Lab 3", 0);
        tempThread.setId(tempThreadId);
        threadList = new ArrayList<>();
        threadList.add(tempThread);
    }

    @Test
    @DisplayName("Testing check id or slug method")
    public void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(tempThreadId)).thenReturn(tempThread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug(tempSlug)).thenReturn(tempThread);
            assertEquals(tempThread, threadController.CheckIdOrSlug(tempThreadId + ""));
            assertEquals(tempThread, threadController.CheckIdOrSlug(tempSlug));
        }
    }

    @Test
    @DisplayName("Testing create post method")
    public void testCreatePost() {
        try (MockedStatic<UserDAO> mockedUserDAO = Mockito.mockStatic(UserDAO.class)) {
            try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                mockedUserDAO.when(() -> UserDAO.Info(tempNick)).thenReturn(tempUser);
                mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug(tempSlug)).thenReturn(tempThread);
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(postList), threadController.createPost(tempSlug, postList));
                assertEquals(tempThread, ThreadDAO.getThreadBySlug(tempSlug));
            }
        }
    }

    @Test
    @DisplayName("Testing create vote method")
    public void testCreateVote() {
        try (MockedStatic<UserDAO> mockedUserDAO = Mockito.mockStatic(UserDAO.class)) {
            try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                mockedUserDAO.when(() -> UserDAO.Info(tempNick)).thenReturn(tempUser);
                Vote vote = new Vote(tempNick, 1);
                mockedThreadDAO.when(() -> ThreadDAO.change(vote, tempThread.getVotes())).thenReturn(tempThread.getVotes());
                mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug(tempSlug)).thenReturn(tempThread);
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(tempThread), threadController.createVote(tempSlug, vote));
            }
        }
    }

    @Test
    @DisplayName("Testing posts method")
    public void testPosts() {
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(tempThreadId)).thenReturn(tempThread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug(tempSlug)).thenReturn(tempThread);
            mockedThreadDAO.when(() -> ThreadDAO.getPosts(tempThreadId, 1, 0, "tree", false)).thenReturn(postList);
            ResponseEntity responseEntity = threadController.Posts(tempThreadId + "", 1, 0, "tree", false);
            assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            assertEquals(postList, responseEntity.getBody());
        }
    }

    @Test
    @DisplayName("Testing info method")
    public void testInfo() {
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(tempThreadId)).thenReturn(tempThread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug(tempSlug)).thenReturn(tempThread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(tempThread), threadController.info(tempThreadId + ""));
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(tempThread), threadController.info(tempSlug));
        }
    }

    @Test
    @DisplayName("Testing change method")
    public void testChange() {
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            Thread newThread = new Thread("Foo", new Timestamp(100), "Lab 2", "My new thread!", "new_thread", "Best of the best", 3);
            newThread.setId(3);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(3)).thenReturn(newThread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("new_thread")).thenReturn(newThread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug(tempSlug)).thenReturn(tempThread);
            mockedThreadDAO.when(() -> ThreadDAO.change(tempThread, newThread)).thenAnswer((invocationOnMock) -> {
                mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("new_thread")).thenReturn(newThread);
                return null;
            });
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(newThread), threadController.change("new_thread", newThread));
            assertEquals(newThread, threadController.CheckIdOrSlug("new_thread"));
        }
    }
}
